const fs = require('fs')
const path = require('path')

const p = '../components/fa'
const np = '../components/fa-n'

//fs.mkdirSync(np)

//(async () => {
for (const type of fs.readdirSync(path.resolve(p))) {
  const typePath = path.resolve(p, type)
  if (fs.lstatSync(typePath).isFile()) continue
  fs.mkdirSync(path.resolve(np, type))
  for (const svg of fs.readdirSync(typePath)) {
    const svgPath = path.resolve(typePath, svg)
    const vuePath = path.resolve(np, type, svg.replace('.svg', '.vue'))
    fs.writeFileSync(
      vuePath,
      '<template>\n  ' +
        fs
          .readFileSync(svgPath)
          .toString('utf-8')
          .replace(/\n<!--(.*|\n)*?-->/, '') +
        '\n</template>\n'
    )
    //console.log(svgPath, vuePath)
  }
}

//})
