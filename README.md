# v-icon

## Project setup
To parse put the font awesome icons/svg folder into the src/components/fa folder.
Check the converting script under src/scripts and run it with node scripts/svg2vue.js

```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```
